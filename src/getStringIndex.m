function ind = getStringIndex(str)
% GETSTRINGINDEX return any number after a '#' symbol in the input string
% or -1 if no index is found.
%
% Usage:
%
% * ind = getStringIndex(str)
%
% Parameters:
%
% * str: the input string.
% * ind: the number read after the sharp sign if any, -1 if not. 
%
% Examples :
%
% * getStringIndex("lkjghlmh lpoihjbl  #15") = 15
% * getStringIndex("lmkdfjm") = -1
% * getStringIndex("kjlhlgh#1jhhgiughuig#56") = [1 56]
%

ind = regexp(str,'#[0-9]*','match');
ind = regexprep(ind,'#','');
if ~isempty(char(ind))
    ind = str2double(ind);
else
    ind = -1;
end
end

