# matlab-unitest

The purpose of this project is to enable ci on matlab projects in order to launch unit tests. A full description of the process is given [here](https://github.com/mathworks/matlab-gitlab-ci-template/blob/main/README.md), this project is just an adaptation of the workflow to work on gitlab.inria.fr with inria matlab licence servers. We use only one simple function written in the `src` folder associated with one unit test put in the `+test` folder 

## General considerations

You first have to enable CI/CD feature for your project as described in the [intro project](https://gitlab.inria.fr/gitlabci_gallery/intro). 

Do not enable shared runners in this context because the shared runner will not be able to connect any matlab licence server. We have to define our own runner on a ci.inria.fr virtual machine.

## Register the matlab server url as a CI variable

To be able to register your matlab version you have to register the Licence server url as a CI variable. Browse to `Settings - CI/CD - Variables` and add a new variable named *MATLAB_SERVER* with the licence server url as value (you can find it in the licence.txt files at the very beginning of the file, it has to be something like: 27000@myserver.inria.fr). Check the "Protect variable" and "Mask variable" properties for security.

## Create your ci.inria.fr VM with gitlab-runner

Follow the instruction of the [CI documentation](https://inria-ci.gitlabpages.inria.fr/doc/page/web_portal_tutorial/#slaves) to create a slave on ci.inria.fr. You can also connect to any of your domains in cloudstack and use a VM you already defined for other projects which avoid creating VM for a single use.

When your VM is created, log into it via ssh as super user (`sudo su` as ci user in ubuntu VM for exemple). Then install/update gitlab-runner with:

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash apt-get update
```

and register your project by using the instructions of the `Settings - CI/CD - Runners -  Specific runners` url and token:

```bash
gitlab-runner register
```

In our example we set the executor as `docker`, the image as `docker:20.10.16` and tags as `matlab-ci`.

## Create your .gitlab-ci.yml

We defined too jobs in our project. The first one is just the execution of the `rand` matlab function to check that the matlab image as been correctly loaded. The second one is executing the unit test of the `+test` folder and report the results and the coverage in the artifacts as junit format (to have more details on the unit tests and coverage reports, see the [pytest-gitlab-reports](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports) project).


