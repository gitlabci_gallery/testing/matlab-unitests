classdef getStringIndexTest < matlab.unittest.TestCase
  
   
    methods (Test)
        function test1(testCase)
            testCase.assertEqual(getStringIndex(...
                "lkjghlmh lpoihjbl  #15"), 15);
            testCase.assertEqual(getStringIndex("lmkdfjm"), -1);
            testCase.assertEqual(getStringIndex(...
                "kjlhlgh#1jhhgiughuig#56"),[1 56]);
        end
    end
    
end